package com.graphics.core;
 
import com.badlogic.gdx.Gdx; 
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align; 
import com.graphics.core.Board.BoardAction;
import com.graphics.core.MyGdxPrj.GameState;
import com.graphics.core.MyGdxPrj.State;

public class GamePanel extends Stage{
	
	public interface PanelAction{
		public void CompleteRecieveFirstProject();
		public void CompleteRecieveSecondProject();
		public void ClickNextResult(GamePanel gp);
		public void ClickPerviousResult(GamePanel gp);
	}
	
	Skin skin; 
	final TextField inputMaxLength, inputMaxResult ;
	public TextField textFieldResult;
	//final Button buttonReadFromFile;
	public final Button buttonStartProject1;
	public final Button buttonStartProject2;
	public final Button buttonHelper;
	public final Label labelStatus;
	BoardAction boardAction;
	Board board;
	GameState gameState;
	PanelAction panelAction;
	public Table resultTable;
	public GamePanel own;
	
	public GamePanel (Skin skin,Board boardO,BoardAction boardActionO ,GameState gameStateO){
		own = this;
		this.skin = skin;
		this.board = boardO; 
		this.gameState = gameStateO;
		this.boardAction = boardActionO; 
 
        Label l2 = new Label("Min Length : " , skin, "default"); 
        l2.setAlignment(Align.left);
        l2.setWidth(100f);  
        l2.setPosition(Gdx.graphics.getWidth() - 270f, Gdx.graphics.getHeight() - 50f);
        addActor(l2);
        inputMaxLength = new TextField("0",skin);
        inputMaxLength.setWidth(100f);
        inputMaxLength.setHeight(30f); 
        inputMaxLength.setPosition(Gdx.graphics.getWidth() - 123f, Gdx.graphics.getHeight() - 53f);
        inputMaxLength.setAlignment(Align.center);
        addActor(inputMaxLength);

        Label l3 = new Label("Max Result : " , skin, "default"); 
        l3.setAlignment(Align.left);
        l3.setWidth(100f);  
        l3.setPosition(Gdx.graphics.getWidth() - 270f, Gdx.graphics.getHeight() - 92f);
        addActor(l3);
        inputMaxResult = new TextField("1000",skin);
        inputMaxResult.setWidth(100f);
        inputMaxResult.setHeight(30f); 
        inputMaxResult.setPosition(Gdx.graphics.getWidth() - 123f, Gdx.graphics.getHeight() - 95f);
        inputMaxResult.setAlignment(Align.center);
        addActor(inputMaxResult);
          
		buttonStartProject1 = new TextButton("Start first project guide ...", skin, "default");
		buttonStartProject1.setWidth(250f);
		buttonStartProject1.setHeight(30f); 
		buttonStartProject1.setPosition(Gdx.graphics.getWidth() - 273f, Gdx.graphics.getHeight() - 140f);
		buttonStartProject1.addListener(new ClickListener(){
            @Override 
            public void clicked(InputEvent event, float x, float y){  
            	setButtonDisable(buttonStartProject1,true);
            	setButtonDisable(buttonStartProject2,true);
            	resultTable.setVisible(false);
            	board.resetBoard();
            	setStatusText("Please Select start point in board ...");
        		gameState.nowState = State.SELECT_START_PRJ1;  
            }
        });
        addActor(buttonStartProject1);
        
        buttonStartProject2 = new TextButton("Start second project guide ...", skin, "default");
        buttonStartProject2.setWidth(250f);
		buttonStartProject2.setHeight(30f); 
		buttonStartProject2.setPosition(Gdx.graphics.getWidth() - 273f, Gdx.graphics.getHeight() - 185f);
		buttonStartProject2.addListener(new ClickListener(){
            @Override 
            public void clicked(InputEvent event, float x, float y){  
            	setButtonDisable(buttonStartProject1,true);
            	setButtonDisable(buttonStartProject2,true);
            	resultTable.setVisible(false);
            	board.resetBoard();
            	setStatusText("Please Select start point in board ...");
        		gameState.nowState = State.SELECT_START_PRJ2;  
            }
        });
        addActor(buttonStartProject2);
        
        buttonHelper = new TextButton("Complete annonation", skin, "default");
        buttonHelper.setWidth(250f);
        buttonHelper.setHeight(30f); 
        buttonHelper.setPosition(Gdx.graphics.getWidth() - 273f, Gdx.graphics.getHeight() - 228f);
        buttonHelper.addListener(new ClickListener(){
            @Override 
            public void clicked(InputEvent event, float x, float y){  
            	if(gameState.nowState == State.SELECT_BLOCK_PRJ1){ 
                	gameState.nowState = State.SELECT_HOLE_PRJ1;
                	setStatusText("Select hole pairs and click \n'Complete anonnate' when end marking.\n " + "");
            	}else if(gameState.nowState == State.SELECT_BLOCK_PRJ2){
            		panelAction.CompleteRecieveSecondProject();
                	setStatusText("Please wait ..."); 
            	}else{
            		panelAction.CompleteRecieveFirstProject();
                	setStatusText("Please wait ...");
            		//setStatusText(" ");
            	}
            }
        });
        addActor(buttonHelper);
        showHelperButton(false);

        Label la = new Label("Authors :\n"
        		+ "Milad Bonkadar\n"
        		+ "Masoud Khanlo" , skin, "default"); 
        la.setAlignment(Align.left);
        la.setWidth(250f); 
        la.setPosition(Gdx.graphics.getWidth() - 289f, 10);
        addActor(la);
        
        Label l1 = new Label("Status :" , skin, "default"); 
        l1.setAlignment(Align.left);
        l1.setWidth(250f); 
        l1.setPosition(Gdx.graphics.getWidth() - 289f, 170);
        addActor(l1);
        
        labelStatus = new Label("Welcome to projects \n" + 
        "Please choose a project ..." , skin, "default");
        labelStatus.setWrap(true);
        labelStatus.setAlignment(Align.center);
        labelStatus.setWidth(250f); 

        ScrollPane scrollPane = new ScrollPane(labelStatus, skin);
        scrollPane.setBounds(Gdx.graphics.getWidth() - 289f, 87, 280, 85); 
        
        addActor(scrollPane); 

        textFieldResult = new TextField("0", skin);
        textFieldResult.setAlignment(Align.center);
        final TextButton nextResult = new TextButton(" > ", skin,"default");
        nextResult.setHeight(42);
        final TextButton perviousResult = new TextButton(" < ", skin,"default");
        perviousResult.setHeight(42);
        resultTable = new Table();
        resultTable.add(perviousResult);
        resultTable.add(textFieldResult).pad(3);
        resultTable.add(nextResult);
        resultTable.setWidth(250f);
        resultTable.setPosition(Gdx.graphics.getWidth() - 273f, Gdx.graphics.getHeight() - 210f); 
        resultTable.setVisible(false);
        addActor(resultTable);

        perviousResult.addListener(new ClickListener(){
            @Override 
            public void clicked(InputEvent event, float x, float y){ 
            	panelAction.ClickPerviousResult(own);
            }
        });

        nextResult.addListener(new ClickListener(){
            @Override 
            public void clicked(InputEvent event, float x, float y){  
            	panelAction.ClickNextResult(own);
            }
        });
        
        textFieldResult.setDisabled(true);
	}
	
	public void setButtonDisable(Button b, boolean disable){ 
    	b.setDisabled(true); 
    	if(disable){
    		b.setColor(Color.TEAL);
    		b.setTouchable(Touchable.disabled);
    	}else{
    		b.setColor(Color.WHITE);
    		b.setTouchable(Touchable.enabled);
    	}
	}
	
	public void setOnPanelAction(PanelAction panelAction){
		this.panelAction = panelAction;
	}
	
	public void render() {
        act();
        draw();
        Gdx.input.setInputProcessor(this);
	}

	public void setStatusText(String text){
		labelStatus.setText(text);
	}

	public void showHelperButton(boolean visible){
		buttonHelper.setVisible(visible);
	}
}
