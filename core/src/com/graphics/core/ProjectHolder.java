package com.graphics.core;

import java.util.ArrayList;

import com.project.algorithm.Cell;
import com.project.algorithm.Hole;

public class ProjectHolder {
	public Cell startCell;
	public Cell endCell;
	public ArrayList<Hole> holes;
	public ArrayList<Cell> blockCells;
	
	
	public ProjectHolder(){
		holes = new ArrayList<Hole>();
		blockCells = new ArrayList<Cell>(); 
	}
}
