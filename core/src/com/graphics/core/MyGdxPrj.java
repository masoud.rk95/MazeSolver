package com.graphics.core;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.graphics.core.Board.BoardAction;
import com.graphics.core.ChessCell.ColorType;
import com.graphics.core.MouseProcess.ClickProcess;
import com.project.algorithm.Algorithm;
import com.project.algorithm.Cell;
import com.project.algorithm.Hole;

public class MyGdxPrj extends ApplicationAdapter implements ClickProcess,GamePanel.PanelAction{ 
	
	public enum State{
		IDLE ,SELECT_START_PRJ1 , SELECT_END_PRJ1 ,
			  SELECT_BLOCK_PRJ1 , SELECT_HOLE_PRJ1 ,
			  SELECT_GET_CALC_PRJ1 ,
			  SELECT_START_PRJ2 , SELECT_END_PRJ2 ,
			  SELECT_BLOCK_PRJ2 , 
		PROCESSING ,
		RECOVERY
	}
	
	public class GameState{
		public State nowState;
		public State recoveryState;
	}
	
	private OrthographicCamera camera;
	protected SpriteBatch batch;
	protected SpriteBatch textBatch;
	private Texture texture;
	private Sprite sprite;
    private Skin skin; 
	private MouseProcess mouseProcess;
	protected GamePanel gamePanel;
    protected Board board;
	public BoardAction boardAction; 
	protected GameState gameState;
 
	ProjectHolder firstProjectData , secondProjectHolder;
	
	@Override
	public void create () { 
		gameState = new GameState();
		gameState.nowState = State.IDLE;
		
		board = new Board();
		Algorithm a = new Algorithm(); 
		a.board = board;
		boardAction = (BoardAction)a;
		
        skin = new Skin(Gdx.files.internal("uiskin.json")); 
    
        camera = new OrthographicCamera(1, 1);
        batch = new SpriteBatch();
 
        texture = new Texture(Gdx.files.internal("data/bg.jpg")); 
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        TextureRegion region = new TextureRegion(texture, 0, 0, 900, 600);

        sprite = new Sprite(region);
        sprite.setSize(1,1);
        sprite.setPosition(-sprite.getWidth()/2, -sprite.getHeight()/2);
 
		mouseProcess = new MouseProcess();
		mouseProcess.setClickProcess(this);
		
		gamePanel = new GamePanel(skin, board, boardAction ,gameState); 
		gamePanel.setOnPanelAction(this);
	}

	@Override
	public void render () {		 
        
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draw static background
        batch.setProjectionMatrix(camera.projection);
        batch.begin();
        sprite.draw(batch); 
        batch.end();

        // Cells render
        board.render();
            
        //hanlde mouse click on chess board
        mouseProcess.process();
        
        //render buttons and all elements in screen
        gamePanel.render(); 

	}
 
	@Override
	public void onTopRightClickCell(int cellX, int cellY) { 
		switch (gameState.nowState) {
			case IDLE:
				break;
			case PROCESSING:
				break;
			case SELECT_START_PRJ1:
				break;
			case SELECT_END_PRJ1:
				break;
			case SELECT_BLOCK_PRJ1:
				int index = existsCellList(firstProjectData.blockCells,cellX,cellY);
				if( index > -1 ){
					firstProjectData.blockCells.remove(index);
					board.setCellColor(cellX, cellY, ChessCell.ColorType.Clear);
					//goToState(State.SELECT_BLOCK_PRJ1,10); 
				}
				break;
			case SELECT_BLOCK_PRJ2:
				int index1 = existsCellList(firstProjectData.blockCells,cellX,cellY);
				if( index1 > -1 ){
					firstProjectData.blockCells.remove(index1);
					board.setCellColor(cellX, cellY, ChessCell.ColorType.Clear);
					//goToState(State.SELECT_BLOCK_PRJ1,10); 
				}
				break;
			case SELECT_HOLE_PRJ1:
				break; 
			case SELECT_END_PRJ2:
				break;
			case SELECT_START_PRJ2:
				break;
			default:
				break; 
		}
	}

	@Override
	public void onTopLeftClickCell(int cellX, int cellY) { 
		switch (gameState.nowState) {
			case IDLE: 
				break;
			case SELECT_START_PRJ1:
				firstProjectData = new ProjectHolder();
				firstProjectData.startCell = new Cell(cellX,cellY);
				board.setCellColor(cellX, cellY, ChessCell.ColorType.Yellow);
				gamePanel.setStatusText("Please select destination point..."); 
				
				goToState(State.SELECT_END_PRJ1,300); 
				break;
			case SELECT_END_PRJ1:
				if(firstProjectData.startCell.X == cellX && 
						firstProjectData.startCell.Y == cellY)
					break;
				firstProjectData.endCell = new Cell(cellX,cellY);
				board.setCellColor(cellX, cellY, ChessCell.ColorType.Yellow); 
				gamePanel.setStatusText("Select block cells and press \n'Complete annonate' button ...\n"
						+ "Left click : Add block cell\n"
						+ "Right click : Remove from block cells");   
				goToState(State.SELECT_BLOCK_PRJ1,300); 

		        gamePanel.showHelperButton(true);
				break;
			case SELECT_BLOCK_PRJ1: 
				if(existsCellList(firstProjectData.blockCells,cellX,cellY) == -1
						&& !( firstProjectData.startCell.X == cellX
							&& firstProjectData.startCell.Y == cellY)
						&& !( firstProjectData.endCell.X == cellX
							&& firstProjectData.endCell.Y == cellY)){
					firstProjectData.blockCells.add(new Cell(cellX,cellY));
					board.setCellColor(cellX, cellY, ChessCell.ColorType.Red);  
					goToState(State.SELECT_BLOCK_PRJ1,50); 
				}
				break;
			case SELECT_HOLE_PRJ1:
				if(existsCellList(firstProjectData.blockCells,cellX,cellY) == -1 &&
						existsHoleList(firstProjectData.holes,cellX,cellY) == -1 
				&& !( firstProjectData.startCell.X == cellX
					&& firstProjectData.startCell.Y == cellY)
				&& !( firstProjectData.endCell.X == cellX
					&& firstProjectData.endCell.Y == cellY)){
					if(firstProjectData.holes.size() == 0){
						Hole h = new Hole(new Cell(cellX,cellY), null);
						firstProjectData.holes.add(h);
						setCellColor(cellX,cellY, ColorType.Sky);
					}else{
						Hole h = firstProjectData.holes.get(firstProjectData.holes.size() - 1);
						if(h.outCell == null){
							h.outCell = new Cell(cellX,cellY);
							setCellColor(cellX,cellY, ColorType.Blue);
						}else{ 
							firstProjectData.holes.add(new Hole(new Cell(cellX,cellY), null));
							setCellColor(cellX,cellY, ColorType.Sky);
						}
					}
					goToState(State.SELECT_HOLE_PRJ1,50);  
				}
				//gamePanel.setStatusText("Please wait for calculating...");
				break;
			case SELECT_START_PRJ2:
				firstProjectData = new ProjectHolder();
				firstProjectData.startCell = new Cell(cellX,cellY);
				board.setCellColor(cellX, cellY, ChessCell.ColorType.Yellow);
				gamePanel.setStatusText("Please select destination point..."); 
				
				goToState(State.SELECT_END_PRJ2,300); 
				break;
			case SELECT_END_PRJ2:
				if(firstProjectData.startCell.X == cellX && 
				firstProjectData.startCell.Y == cellY)
					break;
				firstProjectData.endCell = new Cell(cellX,cellY);
				board.setCellColor(cellX, cellY, ChessCell.ColorType.Yellow); 
				gamePanel.setStatusText("Select block cells and press \n'Complete annonate' button ...\n"
						+ "Left click : Add block cell\n"
						+ "Right click : Remove from block cells");   
				goToState(State.SELECT_BLOCK_PRJ2,300); 
		
		        gamePanel.showHelperButton(true);
				break;
			case SELECT_BLOCK_PRJ2:
				if(existsCellList(firstProjectData.blockCells,cellX,cellY) == -1
				&& !( firstProjectData.startCell.X == cellX
					&& firstProjectData.startCell.Y == cellY)
				&& !( firstProjectData.endCell.X == cellX
					&& firstProjectData.endCell.Y == cellY)){
					firstProjectData.blockCells.add(new Cell(cellX,cellY));
					board.setCellColor(cellX, cellY, ChessCell.ColorType.Red);  
					goToState(State.SELECT_BLOCK_PRJ2,50); 
				}
				break; 
			case PROCESSING:
				break;
			default:
				break; 
		} 
	}
	
	private void goToState(final State s, int delay){

		gameState.nowState = State.RECOVERY;
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			  @Override
			  public void run() {
				gameState.nowState = s;
			  }
			}, delay);
	}
	
	protected int existsCellList(ArrayList<Cell> cells , int x , int y){
		int i = 0;
		for (Cell cell : cells) {
			if(cell.X == x && cell.Y == y)
				return i;
			i++;
		}
		return -1;
	}
	
	protected int existsHoleList(ArrayList<Hole> holes , int x , int y){
		int i = 0;
		for (Hole h : holes) {
			if((h.inCell.X == x && h.inCell.Y == y) ||
			   (h.outCell!= null &&	h.outCell.X == x && h.outCell.Y == y))
				return i;
			i++;
		}
		return -1;
	}
	
	@Override
	public void dispose () {
        batch.dispose();
        texture.dispose();
	}

	protected void setCellColor(int x, int y, ChessCell.ColorType ct){ 
		board.setCellColor(x,y,ct);
	} 
	protected void setCellNumber(int x, int y, int number){ 
		board.setCellNumber(x,y,number);
	} 
	
	protected void setIdleState(){ 
		gameState.nowState = State.IDLE; 
	}

	@Override
	public void CompleteRecieveFirstProject() {

		if(firstProjectData.holes.size() > 0){ 
			Hole h = firstProjectData.holes.get(firstProjectData.holes.size() - 1);
			if(h.outCell == null){
				firstProjectData.holes.remove(firstProjectData.holes.size() - 1);
			}
		}
		
		goToState(State.PROCESSING,0);  
    	boardAction.onStartSolveProject1(
			firstProjectData.startCell,
			firstProjectData.endCell,
			firstProjectData.blockCells,
			firstProjectData.holes,
			Integer.parseInt(gamePanel.inputMaxLength.getText()) ,
			Integer.parseInt(gamePanel.inputMaxResult.getText()),
			gamePanel);
	}

	@Override
	public void CompleteRecieveSecondProject() {
		// TODO Auto-generated method stub

		
		goToState(State.PROCESSING,0);  
    	boardAction.onStartSolveProject2(
			firstProjectData.startCell,
			firstProjectData.endCell,
			firstProjectData.blockCells, 
			Integer.parseInt(gamePanel.inputMaxLength.getText()) ,
			Integer.parseInt(gamePanel.inputMaxResult.getText()),
			gamePanel);
	}

	@Override
	public void ClickNextResult(GamePanel gp) {
		// TODO Auto-generated method stub
		boardAction.GetNextResult(gp);
	}

	@Override
	public void ClickPerviousResult(GamePanel gp) {
		// TODO Auto-generated method stub
		boardAction.GetPerviousResult(gp);
	} 
 
}
