package com.graphics.core;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.project.algorithm.Cell;
import com.project.algorithm.Hole;


public class Board {

	public interface BoardAction { 
		public void onRightClickCell(int cellX , int cellY);
		public void onLeftClickCell(int cellX , int cellY);
		//public void onReadFromFile(ArrayList<GameInput> inputList);
		public void onStartSolveProject1(Cell start, Cell end ,ArrayList<Cell> blockList ,
										 ArrayList<Hole> holeList , int maxWayLength , int maxResults,GamePanel gp); 
		public void onStartSolveProject2(Cell start, Cell end ,ArrayList<Cell> blockList ,
										 int maxWayLength , int maxResults,GamePanel gp); 
		 
		public void GetNextResult(GamePanel tf);
		public void GetPerviousResult(GamePanel tf);
	}

    private ShapeRenderer shapeRenderer;
	public ChessCell [][] cells;
	protected BitmapFont bitmapFont; 
	protected SpriteBatch textBatch;
	
	public Board(){
        shapeRenderer = new ShapeRenderer();
        textBatch = new SpriteBatch();
        
        bitmapFont = new BitmapFont();
        bitmapFont.setColor(Color.WHITE);
        bitmapFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        bitmapFont.getData().setScale(1.5f,1.5f);
        
		cells = new ChessCell[8][8];

		for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) {
            	ChessCell cell = new ChessCell((i+j) % 2 == 0);
            	cell.x = i;
            	cell.y = j;
            	cells[i][j] = cell;
            } 
        }
	}
	
	public void render(){
		Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeType.Filled);
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) {
            	ChessCell cell = cells[i][j];
        		
            	if(cell.color == Color.CLEAR)
            		continue;

                shapeRenderer.setColor(cell.color);
                shapeRenderer.rect(24.5f + 68.9f * i , 25 + 69*j , 68.9f , 69); 
            } 
        } 
        shapeRenderer.end();

        textBatch.begin(); 
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) { 
            	if(cells[i][j].number != -1){ 
            		if(cells[i][j].number < 10)
            			bitmapFont.draw(textBatch, cells[i][j].number + "",
            				52.5f + 68.9f * i , 69 + 69 * j);  
        			else
            			bitmapFont.draw(textBatch, cells[i][j].number + "",
            				45f + 68.9f * i , 69 + 69 * j);  
            				
            	}
            } 
        }
		textBatch.end();
	}
	
	public void resetBoard(){

		for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) {
            	cells[i][j].color = Color.CLEAR;
            	cells[i][j].number = -1;
            } 
        }
	}
	
	public void setCellColor(int cx,int cy,ChessCell.ColorType ct){
		cells[cx][cy].setColor(ct);
	}
	
	public void setCellNumber(int cx,int cy,int number){
		cells[cx][cy].number =  number;
	}

}
