package com.graphics.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

public class MouseProcess {
	interface ClickProcess {
		public void onTopRightClickCell(int cellX ,int cellY);
		public void onTopLeftClickCell(int cellX ,int cellY);
	}
	
	ClickProcess clickProcess;
	
	public void setClickProcess(ClickProcess clickProcess) {
		this.clickProcess = clickProcess;
	}
	
	public void process(){
		// Process click mouse
        if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
        	float x = Gdx.input.getX() - 25.2f;
        	float y = Gdx.input.getY() - 25f;

        	int cx = (int)(x / 68.8f);
        	int cy = (int)(y / 69f); 
        	if((cx >= 0 && cx <= 7) && (7 - cy >= 0 && 7 - cy < 8))
        		clickProcess.onTopLeftClickCell(cx,7-cy);
        }
        if(Gdx.input.isButtonPressed(Input.Buttons.RIGHT)){
        	float x = Gdx.input.getX() - 25.2f;
        	float y = Gdx.input.getY() - 25f;

        	int cx = (int)(x / 68.8f);
        	int cy = (int)(y / 69f); 
        	if((cx >= 0 && cx <= 7) && (7 - cy >= 0 && 7 - cy < 8))
        		clickProcess.onTopRightClickCell(cx,7-cy);
        }
	}
}
