package com.graphics.core;

import com.badlogic.gdx.graphics.Color;

public class ChessCell {
	public Color color;
	public boolean isBlackCell;
	public int x;
	public int y;
	public int number = -1;
	
	public enum ColorType {
		Green , Red , Yellow, Blue ,Sky , Clear
	}
	
	public ChessCell(boolean isBlackCell){
		color = Color.CLEAR;
		this.isBlackCell = isBlackCell;
	}
	
	public void setColor(ColorType ct){
		switch (ct) {
		case Green:
			color = new Color(0, 1, 0, 0.3f);
			break;
		case Red:
			color = new Color(1, 0, 0, 0.5f);
			break;
		case Yellow:
			color = new Color(1, 1, 0, 0.5f);
			break;
		case Blue:
			color = new Color(0, 0, 1, 0.3f);
			break;
		case Sky:
			color = new Color(1f, 1f, 1f, 0.3f);
			break;
		case Clear:
			color = Color.CLEAR;
			break;

		default:
			break;
		}
	}
}
