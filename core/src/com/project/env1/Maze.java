package com.project.env1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.project.algorithm.Cell;
import com.project.algorithm.Hole;
import com.project.algorithm.Path;

public class Maze {
	 public static int chessSize = 8;
	 public static int minLenth = 0;
	 public static int solvedCount = 0;
     public int[][] chess = new int[ chessSize ][ chessSize ];
     private Cell startCell;
     private Cell goalCell;
     private Queue<Path> pathQueue;
     public List<Hole> Holes;
     public List<Path> solved = new ArrayList<Path>();

     public Maze(List<Cell> blocked_cells ,List<Hole> holes, Cell start , Cell goal , int min , int solved)
     {
    	 this.solved.clear();
    	 Maze.minLenth = min;
    	 Maze.solvedCount = solved;
         if(goal==null || start ==null)
             return;
         startCell = start;
         goalCell = goal;
         Holes = holes;
         for (int i = 0; i < chessSize; i++)
         {
             for (int j = 0; j < chessSize; j++)
             {
                 chess[i][j] = 0;
             }
         }
         for (Cell cell : blocked_cells) {
        	 chess[cell.X][cell.Y] = -1;
		}
         pathQueue = new LinkedList<Path>();
         pathQueue.add(new Path(goalCell,startCell,this.solved));
     }

     public List<Path> solveMaze()
     {
         while (!pathQueue.isEmpty() && solved.size()<=Maze.solvedCount )
         {
             Path oldPath = pathQueue.remove();
             if(!oldPath.finished)
                 checkAround(oldPath);
         }
         return solved;
     }

     private void checkAround(Path path)
     {
         Cell posCell = path.path.getLast();
    	 for (Hole holl : Holes) {
    		 if ( holl.inCell.X == posCell.X && holl.inCell.Y == posCell.Y)
                 {
    			 pathQueue.add(new Path(path, new Cell ( holl.outCell.X,holl.outCell.Y ),this.solved));
                 return;
                 }
    	 }
         if (checkCell(posCell.X -1, posCell.Y , path))
         {
             pathQueue.add(new Path(path,new Cell ( posCell.X - 1 ,  posCell.Y ),this.solved));
         }
         if (checkCell(posCell.X + 1, posCell.Y , path))
         {
             pathQueue.add(new Path(path, new Cell ( posCell.X + 1,  posCell.Y ),this.solved));
         }
         if (checkCell(posCell.X, posCell.Y + 1, path))
         {
             pathQueue.add(new Path(path, new Cell ( posCell.X, posCell.Y +1),this.solved));
         }
         if (checkCell(posCell.X, posCell.Y - 1, path))
         {
             pathQueue.add(new Path(path, new Cell ( posCell.X ,  posCell.Y-1 ),this.solved));
         }
     }

     private boolean checkCell(int x, int y , Path p)
     {
         //if (p.start.X == x && p.start.Y == y) return true;
    	 for (Cell cell : p.path) {
    		 if ( cell.X == x && cell.Y == y)
                 return false;
    	 }
         boolean test1 = (x >= 0 && x < chessSize && y >= 0 && y < chessSize );
         return test1 && chess[x][ y] != -1;
     }
}
