package com.project.env2; 

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.project.algorithm.Cell;
import com.project.algorithm.Path;
 

public class Maze {
	 public static int chessSize = 8;
	 public static int minLenth = 0;
	 public static int solvedCount = 0;
     public int[][] chess = new int[ chessSize ][ chessSize ];
     private Cell startCell;
     private Cell goalCell;
     private Queue<Path> pathQueue;
     public List<Path> solved = new ArrayList<Path>();

     enum MOVE
     {
         RIGHT_DOWN , RIGHT_UP,
         LEFT_DOWN,LEFT_UP,
         DOWN_RIGHT,DOWN_LEFT,
         UP_RIGHT,UP_LEFT
     }
     public Maze(List<Cell> blocked_cells , Cell start , Cell goal , int min , int solved)
     {
    	 Maze.minLenth = min;
    	 Maze.solvedCount = solved;
         if(goal==null || start ==null)
             return;
         startCell = start;
         goalCell = goal;
         for (int i = 0; i < chessSize; i++)
         {
             for (int j = 0; j < chessSize; j++)
             {
                 chess[i][j] = 0;
             }
         }
         for (Cell cell : blocked_cells) {
             chess[cell.X][ cell.Y] = -1;
         }
         pathQueue = new LinkedList<Path>();
         pathQueue.add(new Path(goalCell,startCell,this.solved)); 
     }

     public List<Path> solveMaze()
     {
         while (!pathQueue.isEmpty()&& solved.size()<=Maze.solvedCount)
         {
             Path oldPath = pathQueue.remove();
             if(!oldPath.finished)
                 checkAround(oldPath);
         }
         return solved;
     }

     private void checkAround(Path path)
     {
         Cell posCell = path.path.getLast();
         if (checkCell(posCell , posCell.X -2, posCell.Y-1 , path, MOVE.UP_LEFT))
         {
             pathQueue.add(new Path(path,new Cell ( posCell.X -2 , posCell.Y -1),this.solved));
         }
         if (checkCell(posCell, posCell.X -2, posCell.Y+1, path, MOVE.UP_RIGHT))
         {
             pathQueue.add(new Path(path, new Cell ( posCell.X -2, posCell.Y +1),this.solved));
         }
         if (checkCell(posCell, posCell.X+2, posCell.Y-1 , path, MOVE.DOWN_LEFT))
         {
             pathQueue.add(new Path(path, new Cell ( posCell.X+2,  posCell.Y-1 ),this.solved));
         }
         if (checkCell(posCell, posCell.X+2, posCell.Y+1, path, MOVE.DOWN_RIGHT))
         {
             pathQueue.add(new Path(path, new Cell ( posCell.X +2,  posCell.Y +1),this.solved));
         }
         if (checkCell(posCell, posCell.X-1 , posCell.Y-2, path, MOVE.LEFT_UP))
         {
             pathQueue.add(new Path(path, new Cell (posCell.X-1 , posCell.Y -2),this.solved));
         }
         if (checkCell(posCell, posCell.X+1 , posCell.Y-2, path, MOVE.LEFT_DOWN))
         {
             pathQueue.add(new Path(path, new Cell ( posCell.X+1 , posCell.Y-2 ),this.solved));
         }
         if (checkCell(posCell, posCell.X-1, posCell.Y+2 , path, MOVE.RIGHT_UP))
         {
             pathQueue.add(new Path(path, new Cell ( posCell.X-1, posCell.Y+2 ),this.solved));
         }
         if (checkCell(posCell, posCell.X+1, posCell.Y+2 , path,MOVE.RIGHT_DOWN)  )
         {
             pathQueue.add(new Path(path, new Cell ( posCell.X+1, posCell.Y+2 ),this.solved));
         }
     }

     private boolean checkCell(Cell posCell , int x, int y , Path p , MOVE MOVEType)
     {
    	 for (Cell cell : p.path) {
    		 if ( cell.X == x && cell.Y == y)
                 return false;
    	 }
         boolean check1 = (x >= 0 && x < chessSize && y >= 0 && y < chessSize );
         if (check1 && chess[x][y] != -1)
         {
             switch (MOVEType)
             {
                 case DOWN_LEFT:
                     return !(((chess[posCell.X + 1][ posCell.Y] == -1) ||
                         (chess[posCell.X + 2][ posCell.Y] == -1)) && 
                         ((chess[posCell.X ][ posCell.Y-1] == -1) ||
                         (chess[posCell.X + 1][ posCell.Y-1] == -1)));
                 case DOWN_RIGHT:
                     return !(((chess[posCell.X +1][ posCell.Y] == -1) ||
                         (chess[posCell.X+2][ posCell.Y] == -1)) &&
                         ((chess[posCell.X][ posCell.Y+1] == -1) ||
                         (chess[posCell.X+1][ posCell.Y+1] == -1)));
                 case RIGHT_UP:
                     return !(((chess[posCell.X][ posCell.Y+1] == -1) ||
                         (chess[posCell.X][ posCell.Y+2] == -1)) &&
                         ((chess[posCell.X-1][ posCell.Y] == -1) ||
                         (chess[posCell.X-1][ posCell.Y+1] == -1)));
                 case RIGHT_DOWN:
                     return !(((chess[posCell.X][ posCell.Y+1] == -1) ||
                         (chess[posCell.X][ posCell.Y+2] == -1)) &&
                         ((chess[posCell.X+1][ posCell.Y] == -1) ||
                         (chess[posCell.X+1][ posCell.Y+1] == -1)));
                 case LEFT_UP:
                     return !(((chess[posCell.X][ posCell.Y-1] == -1) ||
                         (chess[posCell.X][posCell.Y-2] == -1)) &&
                         ((chess[posCell.X-1][ posCell.Y] == -1) ||
                         (chess[posCell.X-1][ posCell.Y-1] == -1)));
                 case LEFT_DOWN:
                     return !(((chess[posCell.X][ posCell.Y-1] == -1) ||
                         (chess[posCell.X][ posCell.Y-2] == -1)) &&
                         ((chess[posCell.X+1][ posCell.Y] == -1) ||
                         (chess[posCell.X+1][ posCell.Y-1] == -1)));
                 case UP_RIGHT:
                     return !(((chess[posCell.X-1][ posCell.Y] == -1) ||
                         (chess[posCell.X-2][ posCell.Y] == -1)) &&
                         ((chess[posCell.X][ posCell.Y+1] == -1) ||
                         (chess[posCell.X-1][ posCell.Y+1] == -1)));
                 case UP_LEFT:
                     return !(((chess[posCell.X-1][ posCell.Y] == -1) ||
                         (chess[posCell.X-2][ posCell.Y] == -1)) &&
                         ((chess[posCell.X][ posCell.Y-1] == -1) ||
                         (chess[posCell.X-1][ posCell.Y-1] == -1)));
                 default:
                     return false;
             }
         }
         else
         {
             return false;
         }
     }
}
