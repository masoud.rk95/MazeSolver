package com.project.algorithm;

import java.util.LinkedList;
import java.util.List;

import com.project.env1.Maze;

public class Path {
	public int _counter;
    public LinkedList<Cell> path ;
    public boolean finished ;
    public Cell goal;
    public Cell start;
    public Path(Cell g , Cell s ,List<Path> paths)
    {
        _counter = 0;
        path = new LinkedList<Cell>();
        start = s;
        goal = g;
        addCell(start ,paths);
        finished = false;
    }

    public Path(Path old, Cell newCell,List<Path> paths)
    {
        path =new LinkedList<Cell>(old.path);
        goal     = old.goal;
        start    = old.start;
        finished = old.finished;
        _counter = old._counter;
        addCell(newCell,paths);
    }

    public boolean addCell(Cell newCell,List<Path> paths)
    {
        if (finished)
            return false;
        else
        {
            if (newCell.X == goal.X && newCell.Y == goal.Y)
                finished = true;
            newCell.Number = _counter++;
            path.addLast(newCell);
            if (finished && path.size() > Maze.minLenth)
            {
            	paths.add(this);
            }
            return true;
        }
    }
}
