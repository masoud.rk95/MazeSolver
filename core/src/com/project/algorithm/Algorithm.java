package com.project.algorithm;

import java.util.ArrayList;
  
import java.util.List;
import com.graphics.core.Board; 
import com.graphics.core.GamePanel;
import com.graphics.core.MyGdxPrj;
import com.graphics.core.ChessCell.ColorType;
import com.project.env1.Maze;

public class Algorithm extends MyGdxPrj implements Board.BoardAction{

	List<Path> solve;
	ArrayList<Cell> blockCells;
	ArrayList<Hole> holeList;
	Cell start;
	Cell end;
	@Override
	public void onRightClickCell(int cellX, int cellY) {  
	}

	@Override
	public void onLeftClickCell(int cellX, int cellY) {  
	}
	
	@Override
	public void render() { 
		super.render(); 
	} 
	
	@Override
	public void onStartSolveProject1(Cell start, Cell end,
			ArrayList<Cell> blockCells, ArrayList<Hole> holeList ,
			int maxWayLength, int maxResults , GamePanel gp) {
		
        this.blockCells = blockCells;
        this.holeList = holeList;
        this.start = start;
        this.end = end;
        
        Maze m = new Maze(blockCells ,holeList, start , end , maxWayLength , maxResults);
        solve = m.solveMaze();
        
        gp.buttonHelper.setVisible(false);
    	gp.resultTable.setVisible(true);
    	gp.setButtonDisable(gp.buttonStartProject1,false);
    	gp.setButtonDisable(gp.buttonStartProject2,false);
    	
        if(solve.size() > 0){ 
            getResult(0);
        }else{
        	gp.setStatusText("This problem has no result.");
        }
        gp.textFieldResult.setText("0 / "+(solve.size()-1));
	}

	public void getResult(int resultIndex){
		board.resetBoard();
        for (int i = 0; i < solve.get(resultIndex).path.size(); i++) {

        	Cell p = solve.get(resultIndex).path.get(i);
        	setCellColor(p.X, p.Y, ColorType.Green);
        	setCellNumber(p.X, p.Y, p.Number + 1);
		}
        if(holeList != null)
        for (int i = 0; i < holeList.size(); i++) { 
        	Hole h = holeList.get(i);
        	setCellColor(h.inCell.X, h.inCell.Y, ColorType.Blue); 
        	setCellColor(h.outCell.X, h.outCell.Y, ColorType.Blue); 
		}
        
        for (int i = 0; i < blockCells.size(); i++) {
        	Cell p = blockCells.get(i);
        	setCellColor(p.X, p.Y, ColorType.Red); 
		}

    	setCellColor(start.X, start.Y, ColorType.Yellow);
    	setCellColor(end.X, end.Y, ColorType.Yellow);
	}
	
	@Override
	public void onStartSolveProject2(Cell start, Cell end,
			ArrayList<Cell> blockList, int maxWayLength, int maxResults, GamePanel gp) {

        this.blockCells = blockList; 
        this.start = start;
        this.end = end;
        
        com.project.env2.Maze m = 
        		new com.project.env2.Maze(blockList, start , end , maxWayLength , maxResults);
        solve = m.solveMaze();
        
        gp.buttonHelper.setVisible(false);
    	gp.resultTable.setVisible(true);
    	gp.setButtonDisable(gp.buttonStartProject1,false);
    	gp.setButtonDisable(gp.buttonStartProject2,false);
    	
        if(solve.size() > 0){ 
            getResult(0);
        }else{
        	gp.setStatusText("This problem has no result.");
        }
        gp.textFieldResult.setText("0 / "+(solve.size()-1));
	}
 

	@Override
	public void GetNextResult(GamePanel gp) { 
    	int res = Integer.parseInt(gp.textFieldResult.getText().split(" / ")[0]);
		if(res >= solve.size() - 1)
			return;
		gp.textFieldResult.setText(
				(++res) + " / "+( solve.size() - 1)); 
        getResult(res);
	}

	@Override
	public void GetPerviousResult(GamePanel gp) {
    	int res = Integer.parseInt(gp.textFieldResult.getText().split(" / ")[0]);
    	if(res > 0){
    		gp.textFieldResult.setText((--res) + " / "+( solve.size() - 1));
            getResult(res);
    	}
	}
}