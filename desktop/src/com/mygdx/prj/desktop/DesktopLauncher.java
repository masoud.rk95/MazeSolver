package com.mygdx.prj.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration; 
import com.project.algorithm.Algorithm;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Project 2";
		config.useGL30 = true;
		config.width = 900;
		config.height = 600;
		config.resizable = false;
		Algorithm a = new Algorithm(); 
		new LwjglApplication(a , config);
	}
}
